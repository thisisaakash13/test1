import argparse
from logprocessor import logprocessor
import sys
def main():
    parser()

def calc(args):
    process = logprocessor()
    dict=process.processlog(args)
    for key, subdict in dict.items(): 
            print(key+" "+" ".join(str(key) +": "+ str(value) for key, value in subdict.items()))

def parser():    
    parser=argparse.ArgumentParser()
    parser.add_argument('-f',type=str,nargs='+',help='Please specify log files?')
    try:
        args=parser.parse_args()    
        calc(args.f)
    except:
        print("Argument expected:\n"+
              "-h, --help    show this help message and exit\n"+
              "-f  Please specify log files")
        sys.exit(0)
                
if __name__ == '__main__':
    main()
