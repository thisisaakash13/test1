from collections import OrderedDict
import os.path
class logprocessor:
    def processlog(self,filelist):       
        dict={}
        keep_warning = ["warning","error"]      
        
        for filename in filelist:
            if(os.path.exists(filename)):
                with open(str(filename),'r') as file:
                    file = file.readlines()

                for line in file:                
                    for phrase in keep_warning:
                        if phrase in line:
                            date = str(line.split(' ',1)[0])
                            if date not in dict:
                                dict[date]={'warning':0,'error':0}            
                            if phrase in str(line.split()[-1]):        
                                value=dict[date][phrase]+1
                                dict[date].update({phrase:value})
            else:
                print("---------------------------------\n"+
                      "-------------Warning-------------\n"+
                      "---------------------------------\n"+
                      "file "+filename+" doesn't exist\n\n")
       
        dict = OrderedDict(sorted(dict.items()))
        
        return dict
        
