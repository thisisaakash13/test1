import unittest
from logprocessor import logprocessor 

class TestLogprocessor(unittest.TestCase):
    def test_logprocess(self):
        process=logprocessor()
        result1 = process.processlog(["example.log"])
        expected1 = {'2016-12-12': {'warning': 2, 'error': 1} ,'2016-12-13': {'warning': 0, 'error': 1}, '2016-12-14': {'warning': 1, 'error': 1}}
        result2=process.processlog(["example.log","example1.log"])
        expected2={'2016-12-12': {'warning': 2, 'error': 1}, '2016-12-13': {'warning': 0, 'error': 1}, '2016-12-14': {'warning': 2, 'error': 1}, '2016-12-15': {'warning': 1, 'error': 0}, '2016-12-16': {'warning': 0, 'error': 1}}
        result3=process.processlog(["example.log","example1.log","example.log"])
        expected3={'2016-12-12': {'warning': 4, 'error': 2}, '2016-12-13': {'warning': 0, 'error': 2}, '2016-12-14': {'warning': 3, 'error': 2}, '2016-12-15': {'warning': 1, 'error': 0}, '2016-12-16': {'warning': 0, 'error': 1}}

        self.assertDictEqual(result1,expected1)
        self.assertDictEqual(result2,expected2)
        self.assertDictEqual(result3,expected3)

if __name__=='__main__':
    unittest.main()
