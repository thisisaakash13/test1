# yipl-se-log-processor

This repository is created as per the [challenge](https://github.com/younginnovations/problem-statements/tree/master/log-processor) given by young innovations.

1. Make sure you have [python3+](https://www.python.org/downloads/) installed and well functioned
2. Download or clone the project (Extract if necessary) 
3. Open terminal/cmd/powershell and change your working directory to the downloaded files.
4. Make sure your log files are in same directory as the script.

Getting Started
```
python process.py [-h] [-f] <filenames seperated with space.
```

Here, [-h] is an optional argument that gives the necessary detail about the script.
```
python process.py -h
```

To process the log you need to specify -f followed by text log files separated by space if there is more than one.
eg 1
```
python process.py -f example.log
```
Output:
```
2016-12-12 warning: 2 error: 1
2016-12-13 warning: 0 error: 1
2016-12-14 warning: 1 error: 1
```

eg 2
```
python process.py -f example.log example1.log
```
Output:
```
2016-12-12 warning: 2 error: 1
2016-12-13 warning: 0 error: 1
2016-12-14 warning: 2 error: 1
2016-12-15 warning: 1 error: 0
2016-12-16 warning: 0 error: 1
```

Error 1:
```
Argument expected:
-h, --help    show this help message and exit
-f  Please specify log files
```
You need to specify either -h or -f(not both) followed by filename.


Error 2:
```
---------------------------------
-------------Warning-------------
---------------------------------
file <filename.log> doesn't exist
```
Soln 1: Check spelling of filename. You don't need to add extra [ "" / '' ].

Soln 2: Don't use [,] in case of multiple file. Use <space>

Soln 3: Check if file exist in the same directory as script.


For running unit test use one of the following:
```
python -m unittest test_logprocessor.py
```
```
python test_logprocessor.py
```

Please let me know if you like to know more or has any issue following this script.
Thank you